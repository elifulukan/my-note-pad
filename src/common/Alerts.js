import React from 'react';
import AwesomeAlert from 'react-native-awesome-alerts';

export const Alerts = (message, color) => {
    return(
      <AwesomeAlert
            show={true}
            showProgress={true}
            title=""
            message={message}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText="No, cancel"
            confirmText="Oke"
            confirmButtonColor= {color}
            onCancelPressed={() => {
              this.hideAlert();
            }}
            onConfirmPressed={() => {
              this.hideAlert();
            }}
          />
    )
  }
