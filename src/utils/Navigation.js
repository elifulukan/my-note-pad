

export const getNavigationOptionsWithAction = (title, backgroundColor, color,headerLeft) => ({
  title,
  headerStyle: {
    backgroundColor,
    height: 40
  },
  headerTitleStyle: {
    color,
    fontFamily: 'vincHand'
  },
  headerTintColor: color,
  headerLeft
});

export const getDrawerNavigationOptions = (title, backgroundColor, titleColor, drawerIcon) => ({
  title,
  headerStyle: {
    backgroundColor,
  },
  headerTitleStyle: {
    color: titleColor
  },
  headerTintColor: titleColor,
  drawerLabel: title,
  drawerIcon,
});
