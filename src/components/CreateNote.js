import React, { Component } from 'react'
import { StyleSheet, BackHandler, View } from 'react-native'
import { RichTextEditor, RichTextToolbar } from 'react-native-zss-rich-text-editor';

export default class CreateNote extends Component {

    constructor(props) {
        super(props)
        this.state = {
            bgId: props.navigation.getParam('id'),
            txt: ""
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.sendServer();
    }

    sendServer() {
        fetch('https://fierce-bayou-78653.herokuapp.com/savenote', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userid: this.props.username,
                content: this.props.password,
                title: this.props.email,
            }),
        }).then(response => response.json())
            .then(response =>
                (response.success ? this.props.navigation.navigate('Home')
                    : this.props.navigation.navigate('Home', { showAlert: true, alertMess: response.message, alertColor: '#960a21' })))
    }
    componentWillReceiveProps(newProps) {
        if (newProps.navigation.getParam('id') != undefined) {
            this.setState({ bgId: newProps.navigation.getParam('id') })
        }
    }
    _onChangeText = text => {
        alert(text);
        this.setState({ txt: text })
    }

    //*************************** */editore girilen değeri çekip servise gönder
    render() {
        return (
            <View>
                <RichTextEditor
                    ref={(r) => this.richtext = r}
                    titlePlaceholder={'Başlık..'}
                    contentPlaceholder={"Buraya yaz.."}
                    editorInitializedCallback={() => this.onEditorInitialized()}
                    backColor={this.state.bgId}
                    setTextColor={() => this.setTextColor()}
                    onChangeText={(text) => this._onChangeText({ text })}
                />
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})