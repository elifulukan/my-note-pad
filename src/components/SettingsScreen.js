import React from 'react';
import { View, ScrollView, AsyncStorage } from 'react-native';
import TextField from 'react-native-md-textinput';
import AwesomeAlert from 'react-native-awesome-alerts';


export default class SettingsScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      username: this.props.navigation.getParam("username"),
      email: "",
      password: "",
      showAlert: false,
      showPass: true
    }
    this.showPass = this.showPass.bind(this);
  }

  showPass() {
    this.state.press === false
      ? this.setState({ showPass: false, press: true })
      : this.setState({ showPass: true, onPress: false });
  }

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  getUserInfo() {
    fetch('https://fierce-bayou-78653.herokuapp.com/GetUserInfo', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userid: this.props.navigation.getParam('userid')
      }),
    }).then(response => response.json())
      .then(response =>
        (response.success ? this.setState({ username: response.username, email: response.email })
          : this.setState({ showAlert: true, alertMess: response.message, alertColor: '#960a21' }))).catch(function (error) {
            this.setState({ showAlert: true, alertMess: 'Servis hatası. Lütfen daha sonra tekrar deneyin.', alertColor: '#960a21' })
          });
  }
  async componentDidMount() {
    const eMail = "ee"//await AsyncStorage.getItem('Email')
    this.setState({email:eMail})
 }

  render() {
    return (
      <View style={{ flex: 1, margin: 10, marginTop: 20 }}>
        <ScrollView>
          <TextField label={'Kullanıcı Adı'} highlightColor={'#a33b03'} onChangeText={text => this.setState({ username: text })} value={this.state.username} />
          <TextField label={'E-Mail'} highlightColor={'#00BCD4'} onChangeText={text => this.setState({ email: text })} value={this.state.email} />
          <TextField label={'Şifre'} highlightColor={'#a2032d'} onChangeText={text => this.setState({ password: text })} value={this.state.password} secureTextEntry={this.state.showPass} />
        </ScrollView>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title=""
          message={this.state.alertMess}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={false}
          cancelText="Tamam"
          cancelButtonColor={this.state.alertColor}
          onCancelPressed={() => {
            this.hideAlert();
          }}
          onConfirmPressed={() => this.props.navigation.navigate("Login")}

        />
      </View>

    )
  }
}
