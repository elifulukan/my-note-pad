import React, { Component } from 'react';
import { View, Text, Image, TouchableWithoutFeedback, StyleSheet, Animated } from 'react-native';
import CircleButton from 'react-native-circle-button'
import { FingerPress, AddNoteIcon, AllNotes, FavoriteNotes, Settings, ArrowIcon } from '../assets/'

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false
    }
    this.handlePressIn = this.handlePressIn.bind(this);
    this.handlePressOut = this.handlePressOut.bind(this);
  }

  componentWillMount() {
    this.animatedValue = new Animated.Value(1);
  }

  incrementClick = () => {
    this.setState({ clicked: (this.state.clicked ? false : true) })
  }

  handlePressIn() {
    Animated.spring(this.animatedValue, {
      toValue: .3
    }).start()
  }

  handlePressOut() {
    Animated.spring(this.animatedValue, {
      toValue: 1,
      friction:3,
      tension:40
    }).start()
  }

  render() {
    const animatedStyle = {
      transform: [{ scale: this.animatedValue }]
    }
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>

        <View style={{ height: '50%' }}>
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <View style={{ width: 200, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
              <Text style={{ opacity: (this.state.clicked ? 1.0 : 0), fontFamily: 'vincHand', fontSize: 20 }}>
                Favori Notlarım
                </Text>
            </View>
            <View style={{ height: 70, alignItems: 'center', justifyContent: 'flex-start' }}>
              <Image source={ArrowIcon} style={{ height: 30, width: 30, opacity: (this.state.clicked ? 1.0 : 0) }} />
            </View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flexDirection: 'column' }}>
                <View style={{ width: 100, alignItems: 'flex-start', transform: [{ rotate: '300deg' }] }} >
                  <Text style={{ opacity: (this.state.clicked ? 1.0 : 0), fontFamily: 'vincHand', fontSize: 20 }}>
                    Tüm Notlarım
                    </Text>
                </View>
                <View style={{ height: 60, alignItems: 'flex-end', transform: [{ rotate: '300deg' }], justifyContent: 'flex-end' }}>
                  <Image source={ArrowIcon} style={{ height: 30, width: 30, opacity: (this.state.clicked ? 1.0 : 0) }} />
                </View>
              </View>
              <View>
                <CircleButton
                  size={55}
                  primaryColor='#1699BD'
                  secondaryColor='#7BAFC5'
                  iconButtonCenter={FingerPress}
                  onPressButtonCenter={() => this.incrementClick()}
                  iconButtonBottom={Settings}
                  onPressButtonBottom={() => this.props.navigation.navigate("Settings")}
                  iconButtonLeft={AllNotes}
                  onPressButtonLeft={() => this.props.navigation.navigate("AllNotes")}
                  iconButtonRight={AddNoteIcon}
                  onPressButtonRight={() => this.props.navigation.navigate("AddNote")}
                  iconButtonTop={FavoriteNotes}
                  onPressButtonTop={() => this.props.navigation.navigate("FavNotes")}
                />
              </View>
              <View style={{ flexDirection: 'column' }}>
                <View style={{ width: 100, alignItems: 'flex-end', transform: [{ rotate: '60deg' }] }} >
                  <Text style={{ opacity: (this.state.clicked ? 1.0 : 0), fontFamily: 'vincHand', fontSize: 20 }}>
                    Not Ekle
                    </Text>
                </View>
                <View style={{ height: 60, alignItems: 'flex-start', transform: [{ rotate: '60deg' }], justifyContent: 'flex-end' }}>
                  <Image source={ArrowIcon} style={{ height: 30, width: 30, opacity: (this.state.clicked ? 1.0 : 0) }} />
                </View>
              </View>
            </View>
            <View style={{ height: 70, alignItems: 'center', justifyContent: 'flex-start', transform: [{ rotate: '180deg' }] }}>
              <Image source={ArrowIcon} style={{ height: 30, width: 30, opacity: (this.state.clicked ? 1.0 : 0) }} />
            </View>
            <View style={{ width: 200, alignItems: 'center', justifyContent: 'flex-end' }}>
              <Text style={{ opacity: (this.state.clicked ? 1.0 : 0), fontFamily: 'vincHand', fontSize: 20 }}>
                Ayarlarım
                </Text>
            </View>
          </View>
        </View>

        <View style={{ height: '30%', width: '100%' }}>
          <View style={{ flexDirection: 'row', height: '100%', width: '100%' }} >
            <TouchableWithoutFeedback onPressIn={this.handlePressIn} onPressOut={this.handlePressOut} onPress={() => this.props.navigation.navigate("PublishedNotes")}>
              <Animated.View style={[styles.button, animatedStyle]} >
                <Text style={styles.text}>
                  Yayın Akışı</Text>
              </Animated.View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPressIn={this.handlePressIn} onPressOut={this.handlePressOut} onPress={() => this.props.navigation.navigate("AllNotes")}>
              <Animated.View style={[styles.button, animatedStyle]} >
                <Text style={styles.text}>Notlarım</Text>
              </Animated.View>
            </TouchableWithoutFeedback>
          </View>
        </View>
        <View style={{ height: '20%', justifyContent: 'center' }}>
          <View style={{ height: 70, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontFamily: 'vincHand', fontSize: 17 }}>
              Günün Sözü ~
        </Text>
          </View>
          <View style={{ width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
            <Text style={{ fontFamily: 'vincHand', fontSize: 15 }}>
              "Bu dünya, belki de bir başka gezegenin cehennemidir..."
        </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#fff',
    padding: 10,
    marginLeft: 25,
    height: '60%',
    width: '40%',
    borderRadius: 5,
    borderWidth: 0.6,
    borderColor: '#888888',
    // shadowColor: 'rgb(255,0,0)',
    // shadowOpacity: 1,
    // elevation: 8,
    // shadowRadius: 50,
    // shadowOffset: { width: 1, height: 13 },
  },
  text: {
    fontFamily: 'Cochin'
  }
})