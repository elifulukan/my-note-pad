import { KeyboardAvoidingView, Image, Text, View, StyleSheet, TextInput, ImageBackground, YellowBox } from 'react-native';
import React, { Component } from 'react';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Makiko } from 'react-native-textinput-effects';
import ImSource from '../assets/login-background.jpg';
import LogoSrc from '../assets/MyLogo.png';
import MyButtons from './Buttons';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPass: true,
      press: false,
      username: "",
      password: "",
      showAlert: false,
      alertMess: "",
      alertColor: "#D3D3D3",
      spinner:false
    };
    this.showPass = this.showPass.bind(this);
  }
  showPass() {
    this.state.press === false
      ? this.setState({ showPass: false, press: true })
      : this.setState({ showPass: true, onPress: false });
  }

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  render() {
    return (
      <ImageBackground style={styles.picture} source={ImSource}>
        <Spinner
          visible={this.state.spinner}
          textStyle={styles.spinnerTextStyle}
        />
        <View style={styles.logoContainer}>
          <Image source={LogoSrc} style={styles.Image} />
          <Text style={styles.text}>My Writing Pad</Text>
        </View>
        <View style={{ flex: 1, height: '40%', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ padding: 16 }}>
            <Makiko
              label={'KULLANICI ADI'}
              iconClass={FontAwesomeIcon}
              iconName={'grav'}
              iconColor={'#1c1c1c'}
              iconSize={35}
              inputStyle={{ color: '#2c2c2c' }}
              onChangeText={text => this.setState({ username: text })}
            />
            <Makiko
              style={{ marginTop: 4 }}
              label={'ŞİFRE'}
              iconClass={FontAwesomeIcon}
              iconName={'rocket'}
              iconColor={'#1c1c1c'}
              inputStyle={{ color: '#2c2c2c' }}
              secureTextEntry={this.state.showPass}
              onChangeText={text => this.setState({ password: text })}
            />
          </View>
        </View>
        <MyButtons alert={this} navigation={this.props.navigation} username={this.state.username} password={this.state.password} />
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title=""
          message={this.state.alertMess}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={false}
          cancelText="Tamam"
          cancelButtonColor={this.state.alertColor}
          onCancelPressed={() => {
            this.hideAlert();
          }}
          onConfirmPressed={() => this.props.navigation.navigate("Login")}

        />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  picture: {
    flex: 1,
    width: null,
    height: null,
  },
  logoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Image: {
    height: 80,
    width: 80,
  },
  text: {
    color: '#5D5C5C',
    backgroundColor: 'transparent',
    fontFamily: "vincHand",
    marginTop: 20,
    fontSize: 20,
  },
  txtInput: {
    width: '80%',
    fontFamily: 'vincHand'
  },
  spinnerTextStyle:{
    color: '#FFF'
  }
});