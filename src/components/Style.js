
export const styles = StyleSheet.create({
    buttonLabel: {
      fontSize: 20,
      color: '#5d5c5c',
      fontFamily: 'vincHand',
      alignItems: 'center',
      justifyContent: 'center',
  
    },
    btnEye: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      height: 30,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'rgba(203, 203, 203, 0.5)',
    },
  });