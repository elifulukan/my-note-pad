import React, { Component } from 'react'
import { View, FlatList, ActivityIndicator, Text } from "react-native";
import { ListItem, SearchBar } from "react-native-elements";
import Ionicons from 'react-native-vector-icons/Ionicons'
export default class PublishedNotes extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            data: [],
            page: 0,
            error: null,
            refreshing: false,
        };
    }

    componentDidMount() {
        this.makeRemoteRequest();
    }


    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "14%"
                }}
            />
        );
    };

    makeRemoteRequest = () => {
        const { page } = this.state;
        // const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
        const url = 'https://fierce-bayou-78653.herokuapp.com/GetPublicNotes'
        this.setState({ loading: true });
        fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                page: page
            })
        }).then(res => res.json())
            .then(res => {
                this.setState({
                    data: page === 0 ? res.allNotes : [...this.state.data, ...res.allNotes],
                    error: !res.success || null,
                    loading: false,
                    refreshing: false
                });
            })
            .catch(error => {
                this.setState({ error, loading: false });
            });
    };

    handleRefresh = () => {
        this.setState(
            {
                page: 0,
                refreshing: true
            },
            () => {
                this.makeRemoteRequest();
            }
        );
    };

    handleLoadMore = () => {
        this.setState(
            {
                page: this.state.page + 1
            },
            () => {
                this.makeRemoteRequest();
            }
        );
    };

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "14%"
                }}
            />
        );
    };

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 10,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    renderHeader = () => {
        return <SearchBar placeholder="Ara..." round
            lightTheme
            inputStyle={{ backgroundColor: '#FFFFFF' }}
        // onChangeText={this.updateSearch}
        // value={search} 
        />;
    };

    render() {
        return (
            // <List>
            <FlatList
                data={this.state.data}
                renderItem={({ item }) => (
                    <ListItem
                        roundAvatar
                        title={item.title}
                        subtitle={item.content}
                        avatar={{ uri: item.thumbnail }}
                        containerStyle={{ borderBottomWidth: 0 }}
                        // rightTitle={`${item.username} - ♥ ${item.likeCount} ☺ ${item.commentCount}`}
                        rightTitle={
                                <Ionicons name='ios-heart-empty' />                              
                        }
                        subtitleNumberOfLines={2}
                        rightTitleNumberOfLines={2}
                    // wrapperSyyle= {{
                    //     flexDirection: 'row',
                    //     marginLeft: 10,
                    //     alignItems: 'center',
                    //     flex: 1,
                    //   }}
                    />
                )}
                keyExtractor={item => item._id}
                ItemSeparatorComponent={this.renderSeparator}
                ListHeaderComponent={this.renderHeader}
                ListFooterComponent={this.renderFooter}
                onRefresh={this.handleRefresh}
                refreshing={this.state.refreshing}
                onEndReached={this.handleLoadMore}
                onEndReachedThreshold={10}
            />
            // </List>
        )
    }
}
