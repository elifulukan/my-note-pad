import React from 'react'
import { TouchableOpacity } from 'react-native'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';
import DrawerScreen from './DrawerNav';



const NavStack = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null
      }
    },
    DrawerOpen: {
      screen: DrawerScreen,

    },
    Register: {
      screen: RegisterScreen,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null
      }
    },
  
  },

  {
    initialRouteName: 'Login',
    navigationOptions: ({ navigation }) => ({
      headerLeft: <TouchableOpacity onPress={() => navigation.openDrawer()}>
        <Ionicons name='md-menu' size={35} style={{ marginLeft: 5, padding: 5, color: "white" }} />
      </TouchableOpacity>
      //<Ionicons name='ios-menu-outline' size={35} style={{ marginLeft:5, padding:5, color: "white" }} onPress={ () => navigation.openDrawer() } />
    })
  }
);




const AppContainer= createAppContainer(NavStack);
export default AppContainer;
/*
const MainNavigator = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      headerMode: 'none',
      header: null,
      navigationOptions: {
          header: null
      }
    },

    DrawerOpen: {
        screen: DrawerScreen

    },
    Register:{
      screen: RegisterScreen,
      headerMode: 'none',
      header: null,
      navigationOptions: {
          header: null
      }
    }
   
  },
  {
    initialRouteName: 'Login',
   navigationOptions: ({ navigation }) => ({
           headerLeft:  <TouchableOpacity onPress={() => navigation.openDrawer() }>
          <Ionicons name='ios-menu-outline' size={35} style={{ marginLeft:5, padding:5, color: "white" }} />
        </TouchableOpacity>
        //<Ionicons name='ios-menu-outline' size={35} style={{ marginLeft:5, padding:5, color: "white" }} onPress={ () => navigation.openDrawer() } />
           })


  }
);
 */