import React, { Component } from "react";
import { FlatList, Text, View, YellowBox } from "react-native";
import { Card } from "react-native-elements";

const data = require('../common/NoteBackground.json');

YellowBox.ignoreWarnings([
  "Warning: Failed child context type: Invalid child context",
]);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: data
    };
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: '60%' }}>
          <FlatList
            horizontal
            data={this.state.data}
            renderItem={({ item: rowData }) => {
              return (
                <Card
                  title={rowData.title}
                  image={null}
                  containerStyle={{ padding: 0, width: 300, height: 350, backgroundColor: rowData.title }}
                  navigation={this.props.navigation}
                >
                </Card>
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
        <View style={{ height: '40%', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
          <Text>Lütfen arka plan seçiniz..</Text>

        </View>
      </View>

    );
  }
}