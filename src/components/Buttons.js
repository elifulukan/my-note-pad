import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, AsyncStorage } from 'react-native';

const styles = StyleSheet.create({
  buttonLabel: {
    fontSize: 20,
    color: '#5d5c5c',
    fontFamily: 'vincHand',
    alignItems: 'center',
    justifyContent: 'center',

  },
  btnEye: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(203, 203, 203, 0.5)',
  },
});

export default class Buttons extends Component {
  sendServer() {
    fetch('https://fierce-bayou-78653.herokuapp.com/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.props.username,
        password: this.props.password,
        email: this.props.email,
      }),
    }).then(response => response.json())
      .then(response =>
        (response.success ?
          this.props.navigation.navigate("Home")
          : this.props.alert.setState({ showAlert: true, alertMess: response.message, alertColor: '#960a21'})));
  }
  paramValidate(){
    this.props.alert.setState({spinner:true})
    if(!this.props.username || !this.props.password){
      this.props.alert.setState({showAlert:true, alertMess: 'Lütfen alanları doldurunuz!',alertColor: '#960a21',spinner:false})
      return;
    }
    this.sendServer();
    this.props.alert.setState({ spinner:false })

  }
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
          <TouchableOpacity style={[styles.btnEye, { width: '99%' }]} onPress={() => this.paramValidate()} >
            <Text style={styles.buttonLabel} >Giriş</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
          <TouchableOpacity style={styles.btnEye} onPress={() => this.props.navigation.navigate('Register')} >
            <Text style={styles.buttonLabel} >Kayıt</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
