import React from 'react'
import {View,Text} from 'react-native'

export default class FavNotesScreen extends React.Component{
  render(){
    return(
      <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
        <Text>
          Your favorite notes will be seen in here!
        </Text>
      </View>
    )
  }
}
