import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native';
import { RegisterBackground } from '../assets/';
import TextField from 'react-native-md-textinput';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';
const styles = StyleSheet.create({
  text: {
    color: '#2c2c2c',
    backgroundColor: 'transparent',
    fontFamily: 'vincHand',
    marginTop: 20,
    fontSize: 20,
  },
  picture: {
    flex: 1,
    width: null,
    height: null
  },
  btnEye: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: '6%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'rgba(42, 112, 153, 0.4)',
  },
  txtInput: {
    width: '100%',
    fontFamily: 'vincHand',
  },
  errorText: {
    color: 'red',
    top: 5
  },
  spinnerTextStyle: {
    color: '#FFF'
  }
});

function goServer(props) {
  fetch('https://fierce-bayou-78653.herokuapp.com/register', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      userid: 1,
      username: props.state.username,
      password: props.state.password,
      email: props.state.email
    }),
  }).then(function (response) {
    return response.json()
  }).then(function (response) {
    response.success ? props.setState({ loginSuccess: true, showAlert: true, alertMess: response.message, alertColor: "#5d8aa8", confirmColor: "#00b200",spinner:false })
      : props.setState({ loginSuccess: false, showAlert: true, alertMess: response.message, alertColor: "#960a21",spinner:false })
  }).catch(function (error) {
    props.setState({
      spinner: false
    });
    alert('There has been a problem with operation: ' + error.message);
    throw error;
  });
}

function paramValidation(props) {
  if (!(props.state.username && props.state.password && props.state.email && props.state.passwordAgain)) {
    alert('Lütfen tüm alanları doldurunuz!')
    return
  }
  if (props.state.password.length < 5) {
    props.setState({ showLengthError: true })
    return
  }
  props.setState({
    spinner: true
  });
  goServer(props);
}

export default class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPass: true,
      press: false,
      username: '',
      email: '',
      password: '',
      passwordAgain: '',
      showAlert: false,
      alertMess: "",
      alertColor: "#010101",
      loginSuccess: false,
      confirmColor: "#ffffff",
      showError: false,
      showLengthError: false,
      emailError: false,
      spinner: false
    };
    this.showPass = this.showPass.bind(this);
  }

  showPass() {
    this.state.press === false
      ? this.setState({ showPass: false, press: true })
      : this.setState({ showPass: true, onPress: false });
  }

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  passwordValidation() {
    if (this.state.password.length > 5) {
      this.setState({ showLengthError: false })
    }
    if (this.state.passwordAgain && this.state.password != this.state.passwordAgain) {
      this.setState({ showError: true })
    }
    else
      this.setState({ showError: false })
  }
  emailValidation = () => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.email) === false) {
      this.setState({ emailError: true })
      return false;
    }
    else {
      this.setState({ emailError: false })
    }
  }
  render() {
    return (
      <ImageBackground style={styles.picture} source={RegisterBackground}>
         <Spinner
          visible={this.state.spinner}
          textStyle={styles.spinnerTextStyle}
        />
        <View style={{ height: '10%', justifyContent: 'center', alignItems: 'center' }}>
          <Text style={styles.text}>
            KAYIT OL {this.state.dataSource}
          </Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ height: '50%', width: '80%' }}>
            <TextField
              label={'Kullanıcı Adı'}
              labelColor={'#686868'}
              highlightColor={'#595959'}
              autoCorrect={false}
              value={this.state.username}
              onChangeText={text => this.setState({ username: text })} />
            <TextField
              label={'E-Mail'}
              labelColor={'#686868'}
              highlightColor={'#00BCD4'}
              autoCorrect={false}
              value={this.state.email}
              onChangeText={text => this.setState({ email: text })}
              onBlur={() => this.emailValidation()}
            />
            {this.state.emailError && <Text style={styles.errorText}>E-mail formatı yanlış ! Örn: xxx@gmail.com </Text>}

            <TextField
              label={'Şifre'}
              labelColor={'#686868'}
              highlightColor={'#a2032d'}
              secureTextEntry={this.state.showPass}
              value={this.state.password}
              onChangeText={text => { this.setState({ password: text }, () => { this.passwordValidation(); }); }} />
            <TextField
              label={'Şifre Tekrar'}
              labelColor={'#686868'}
              highlightColor={'#6d273a'}
              secureTextEntry={this.state.showPass}
              value={this.state.passwordAgain}
              onChangeText={text => { this.setState({ passwordAgain: text }, () => { this.passwordValidation(); }); }}
            />
            {this.state.showError && <Text style={styles.errorText}>Şifreler aynı olmalıdır.</Text>}
            {this.state.showLengthError && <Text style={styles.errorText}>Şifre uzunluğu 5'ten küçük olamaz!</Text>}

          </View>
          <View style={{ height: '38%', width: '90%', justifyContent: 'flex-end' }}>
            <Text style={{ fontStyle: 'italic', color: '#686868' }}>*Şifre uzunluğu 5'ten küçük olamaz.</Text>

          </View>
        </View>
        <TouchableOpacity style={styles.btnEye} onPress={() => paramValidation(this)}>
          <Text style={styles.text} >KAYIT</Text>
        </TouchableOpacity>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title=""
          message={this.state.alertMess}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={this.state.loginSuccess}
          cancelText="Tamam"
          confirmText="Giriş Yap"
          cancelButtonColor={this.state.alertColor}
          confirmButtonColor={this.state.confirmColor}
          onCancelPressed={() => {
            this.hideAlert();
          }}
          onConfirmPressed={() => this.props.navigation.navigate("Login")}

        />
      </ImageBackground>
    )
  }
}
