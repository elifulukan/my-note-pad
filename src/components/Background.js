import React, { Component } from 'react';
import { StyleSheet, Image, View, ImageBackground } from 'react-native';
import ImSource from '../assets/pexels-photo.jpg';
import LogoSrc from '../assets/MyLogo.png';

const styles = StyleSheet.create({
  picture: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
  },
  container: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Image: {
    height: 80,
    width: 80,
  },
});

export default class Background extends Component {
  render() {
    return (
      <ImageBackground style={styles.picture} source={ImSource}>
        <View style={styles.container}>
        <Image source={LogoSrc} style={styles.Image} />
      </View>
      </ImageBackground>
    );
  }
}
