import { createDrawerNavigator, DrawerItems } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import React from 'react';
import { TouchableOpacity } from 'react-native'
import { getNavigationOptionsWithAction, getDrawerNavigationOptions } from '../utils/Navigation';
import HomeScreen from './HomeScreen';
import SettingsScreen from './SettingsScreen'
import AllNotesScreen from './AllNotesScreen'
import FavNotesScreen from './FavNotesScreen'
import AddNote from './AddNote'
import LogoutScreen from './LogoutScreen'
import CreateNoteScreen from './CreateNote';
import PublishedNotes from './PublishedNotes'

const getDrawerItem = navigation => (
  <TouchableOpacity onPress={() => { navigation.state.isDrawerOpen ? navigation.closeDrawer() : navigation.openDrawer() }}>
    <Ionicons name='ios-menu' size={35} style={{ marginLeft: 5, padding: 5, color: "white" }} />
  </TouchableOpacity>
);

const getOcticonsIcon = (iconName, tintColor) => <Octicons name={iconName} size={25} color={tintColor} />;
const getAntDesignIcon = (iconName, tintColor) => <AntDesign name={iconName} size={24} color={tintColor} />;
const getDrawerIcon = (iconName, tintColor) => <Ionicons name={iconName} size={30} color={tintColor} />;

const homeDrawerIcon = ({ tintColor }) => getAntDesignIcon('home', tintColor);
const notesDrawerIcon = ({ tintColor }) => getDrawerIcon('md-book', tintColor);
const favDrawerIcon = ({ tintColor }) => getDrawerIcon('md-heart-half', tintColor);
const addDrawerIcon = ({ tintColor }) => getDrawerIcon('ios-add', tintColor);
const settingsDrawerIcon = ({ tintColor }) => getDrawerIcon('ios-settings', tintColor);
const logoutDrawerIcon = ({ tintColor }) => getAntDesignIcon('logout', tintColor);
const pubNotesIcon = ({ tintColor }) => getOcticonsIcon('broadcast', tintColor);

const homeNavOptions = getDrawerNavigationOptions('Ana Sayfa', '#4C7B81', 'white', homeDrawerIcon);
const notesNavOptions = getDrawerNavigationOptions('Notlar', '#4C7B81', 'white', notesDrawerIcon);
const favNavOptions = getDrawerNavigationOptions('Favoriler', '#4C7B81', 'white', favDrawerIcon);
const addNavOptions = getDrawerNavigationOptions('Not Ekle', '#4C7B81', 'white', addDrawerIcon);
const settingsNavOptions = getDrawerNavigationOptions('Ayarlar', '#4C7B81', 'white', settingsDrawerIcon);
const logoutNavOptions = getDrawerNavigationOptions('Çıkış Yap', '#4c7b81', 'white', logoutDrawerIcon);
const pubNotesOptions = getDrawerNavigationOptions('Yayın Akışı', '#4c7b81', 'white', pubNotesIcon);
const Drawer = createDrawerNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: homeNavOptions

  },
  PublishedNotes: {
    screen: PublishedNotes,
    navigationOptions: pubNotesOptions
  },
  AllNotes: {
    screen: AllNotesScreen,
    navigationOptions: notesNavOptions
  },
  AddNote: {
    screen: AddNote,
    navigationOptions: addNavOptions
  },
  FavNotes: {
    screen: FavNotesScreen,
    navigationOptions: favNavOptions
  },
  Settings: {
    screen: SettingsScreen,
    navigationOptions: settingsNavOptions
  },
  Logout: {
    screen: LogoutScreen,
    navigationOptions: logoutNavOptions
  },
  CreateNote: {
    screen: CreateNoteScreen,
    navigationOptions: {
      drawerLabel: () => null
    }
  }

},
  {

    contentOptions: {
      activeTintColor: '#3f7ea3',
    },

  });

Drawer.navigationOptions = ({ navigation }) => getNavigationOptionsWithAction('', '#3f7ea3', 'white', getDrawerItem(navigation));

export default Drawer;
