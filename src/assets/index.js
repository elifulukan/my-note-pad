export const FingerPress=require('./finger-press.png');
export const AddNoteIcon=require('./add-note.png')
export const AllNotes=require('./all-notes.png')
export const FavoriteNotes=require('./favorite-notes.png')
export const Settings=require('./settings-icon.png')
export const ArrowIcon=require('./arrow-down-icon.png')
export const HomeBackground=require('./home-background2.png')
export const RegisterBackground= require('./register-background.jpg');
