import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Text, View } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LoginScreen from './src/components/LoginScreen';
import RegisterScreen from './src/components/RegisterScreen';
import DrawerScreen from './src/components/DrawerNav';

const styles = StyleSheet.create({
  icon: {
    marginLeft: 5,
    padding: 5,
    color: "white"
  },
  navBarRightButton: {
    paddingRight: 8,
    width: 100,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  text: {
    color:['transparent', 'rgba(255, 255, 255, 0.8)'],
    fontSize: 10,
    fontWeight:'bold'
  },
  circle: {
    marginTop:10,
    width: 13,
    height: 13,
    borderRadius: 13/2,
    alignItems:'center',
    backgroundColor:'#ce0000'
 }
});

const NavStack = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null
      }
    },
    DrawerOpen: {
      screen: DrawerScreen,
      navigationOptions: {
        headerRight: (
          <TouchableOpacity style={styles.navBarRightButton} onPress={() => alert('This is a button!')}>
            <Ionicons
              name="ios-notifications-outline"
              color="white"
              size={30}
              style={styles.icon}
            />
            {/* Bildirimler alınmaya başlayınca dbden anlık çekilecek */}
            {/* <View style={styles.circle}> */}
            <Text style={styles.text}>
              3
          </Text>
          {/* </View> */}
          </TouchableOpacity>
        )
      }
    },
    Register: {
      screen: RegisterScreen,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null
      }
    },
  },
  {
    initialRouteName: 'Login',
    navigationOptions: ({ navigation }) => ({
      headerLeft: <TouchableOpacity onPress={() => navigation.openDrawer()}>
        <Ionicons name='md-menu' size={35} style={styles.icon} />
      </TouchableOpacity>
    })
  }
);

const AppContainer = createAppContainer(NavStack);

export default class App extends Component {
  // someEvent() {
  //   this.navigator &&
  //     this.navigator.dispatch(
  //       NavigationActions.navigate({ routeName: someRouteName })
  //     );
  // }
  render() {
    return (
      <AppContainer
        ref={nav => {
          this.navigator = nav;
        }}
      />
    );
  }
}

